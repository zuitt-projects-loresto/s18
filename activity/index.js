function Pokemon(name, lvl, hp){

			// Properties
			this.name = name;
			this.lvl = lvl;
			this.health = hp * 2;
			this.attack = lvl;
			this.maxhealth = hp * 2;
			this.attackcount = 0;
			this.crithit = lvl * 2;
			this.potioncount = 2;

			// methods
			this.tackle = function(target){
				this.attackcount ++;
				if ((target.health%5 <= 4) && (target.health%5 >= 3)) {
					console.log(`${this.name} tackled ${target.name}`);
					console.log(`Critical Hit!`);
					console.log(`${target.name}'s health is reduced by ${this.crithit}`);
					target.health = target.health - this.crithit;
					console.log(`${target.name}'s health is now ${target.health}`);	
				} else if ((target.health%5 <= 2) && (target.health%5 >= 1) && (this.attackcount%2 == 0)) {
					console.log(`${this.name} tackled ${target.name}`);
					console.log(`${target.name} evaded the attack.`);
				} else {
					console.log(`${this.name} tackled ${target.name}`);
					console.log(`${target.name}'s health is reduced by ${this.attack}`);
					target.health = target.health - this.attack;
					console.log(`${target.name}'s health is now ${target.health}`);	
				};
				
				if (target.health < 10){
					target.faint()
				} else if (target.health < 15 && target.health > 10){
					console.log(`We still have a few more left.`);
				} else if (target.health < 25 && target.health > 20){
					console.log(`${target.name}! Hold on, we can't lose in this battle!`);
				}else if (target.health < (target.maxhealth*0.75) && target.health > (target.maxhealth*0.7)){
					console.log(`${this.name}, is that all you got?`);
				} else if (this.attackcount == 1){
					console.log(`${this.name}, is that it? This is just a minor scratch.`);
				} else if (this.attackcount == 3){
					console.log(`We are just getting started!`);
				}
			};
			
			this.potion = function(){
				if (this.potioncount > 0){
					this.potioncount --;
					console.log(`${this.name} drank a potion.`);
					this.health = this.health + 50;
					console.log(`${this.name}'s health was restored by 50.`);
				} else {
					console.log(`You have no potion left.`);
				}
			}

			this.faint = function(){
				console.log(`${this.name} fainted.`)
				console.log(`Game over!`)
			};
	};

let machamp = new Pokemon("Machamp", 23, 250);
let ninetales = new Pokemon("Ninetales", 24, 220);